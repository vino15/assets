//Function with optional value
function disp_details(id: number, name: string, mail_id?: string) {
    console.log("ID:", id);
    console.log("Name", name);

    if (mail_id != undefined)
        console.log("Email Id", mail_id);
}

disp_details(123, "John");
disp_details(111, "mary", "mary@xyz.com");

//Function with default value
function calculate_discount(price: number, rate: number = 0.50) {
    var discount = price * rate;
    console.log("Discount Amount: ", discount);
}

calculate_discount(12)

//Function overloading

//union type and interface

interface RunOptions { 
    program:string; 
    commandline:string[]|string|(()=>string); 
 } 
 
 //commandline as string 
 var options:RunOptions = {program:"test1",commandline:"Hello"}; 
 console.log(options.commandline)  
 
 //commandline as a string array 
 options = {program:"test1",commandline:["Hello","World"]}; 
 console.log(options.commandline[0]); 
 console.log(options.commandline[1]);  
 
 //commandline as a function expression 
 options = {program:"test1",commandline:()=>{return "**Hello World**";}}; 
 
 var fn:any = options.commandline; 
 console.log(fn());

 //interface and array

 interface namelist { 
    [index:number]:string|number
 } 
 
 var list2:namelist = ["John",1,"Bran"] //Error. 1 is not type string  
 interface ages { 
    [index:number]:number|string
 } 
 
//  var agelist:ages; 
//  agelist["John"] = 15   // Ok 
//  agelist[2] = "nine"   // Error

 //interface and inheritance

 interface Person { 
    age:number 
 } 
 
 interface Musician extends Person { 
    instrument:string 
 } 
 
 var drummer = <Musician>{}; 

 drummer.age = 27 
 drummer.instrument = "Drums" 
 console.log("Age:  "+drummer.age);
 console.log("Instrument:  "+drummer.instrument)