var tween = gsap.to(".box", {
    // x: 100, //normal value
    // x:"random(-100, 100, 3)",
    x:gsap.utils.random(0, 1000),
    staggers : .1,
    delay:.5,
    y: function(index, target, targets) { //function-based value
        return index * 50; 
    },
    duration: 2
});
document.querySelector("#play").onclick = () => tween.play();
document.querySelector("#pause").onclick = () => tween.pause();
document.querySelector("#resume").onclick = () => tween.resume();
document.querySelector("#reverse").onclick = () => tween.reverse();
document.querySelector("#restart").onclick = () => tween.restart();