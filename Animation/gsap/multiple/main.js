//SEQUENCE

// let tl = gsap.timeline(); //create the timeline
// tl.to(".class1", {x: 100}) //start sequencing
//   .to(".class2", {x: 200})
//   .to(".class3", {rotation: 180});

//KEYFRAMES

gsap.to(".class", {keyframes: [
    {x: 100, duration: 1},
    {y: 200, duration: 1, delay: 0.5}, //create a 0.5 second gap
    {rotation: 360, duration: 2, delay: -0.25} //overlap by 0.25 seconds
  ], ease: "power3.inOut"});