function animateText(){
  let heading = document.getElementsByClassName('text-animation-heading')[0]
   let text = heading.textContent

   let textSplit = text.trim().split('')

   heading.textContent = ''

   for (let index = 0; index < textSplit.length; index++) {
    heading.innerHTML += `<span>${textSplit[index]}</span>`
   }

   var charIndex = 0

   var timer = setInterval(onTick,50)

   function onTick(){
       let span = heading.querySelectorAll('span')[charIndex]
       span.classList.add('fade')
       charIndex ++;
       if(charIndex === textSplit.length)
        {
            complete()
            return;
        }   
   }

   function complete(){
       clearInterval(timer)
   }
   
}

animateText()