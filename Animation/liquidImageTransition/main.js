var myAnimation = new hoverEffect({
    parent: document.querySelector('.distortion'),
    intensity: 0.3,
    image1: 'images/balloon.jpg',
    image2: 'images/balloon2.jpg',
    image3:'images/balloon2.jpg',
    displacementImage: 'images/waves/heightMap.png'
});