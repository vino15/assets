// const path = require('path'); 
// const HtmlWebpackPlugin = require('html-webpack-plugin'); 

// module.exports = { 
//     entry: path.join(__dirname, 'src', 'index.js'), 
//     output: { 
//         path: path.join(__dirname, 'build'), 
//         filename: 'index.bundle.js' 
//     }, 
//     mode: process.env.NODE_ENV || 'development', 
//     resolve: { 
//         modules: [path.resolve(__dirname, 'src'), 'node_modules'] 
//     }, 
//     devServer: { 
//         contentBase: path.join(__dirname, 'src') 
//     }, 
//     plugins: [
//         new HtmlWebpackPlugin({ template: path.join(__dirname, 'src', 'index.html') })
//     ] 
// };

import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

module.exports = {  
    entry: path.join(__dirname,'src','index.js'),  
    output: {   
        path: path.join(__dirname,'build'),    
        filename: 'index.bundle.js'  
    },  
    mode: process.env.NODE_ENV || 'development',  
    resolve: {    
        modules: [
            path.resolve(__dirname, 'src'), 
            'node_modules'
        ]  },  
        devServer: {    
            contentBase: path.join(__dirname,'src')  
        },  
        module: {    
            rules: [      
                {        
                    // this is so that we can compile any React,        
                    // ES6 and above into normal ES5 syntax        
                    test: /\.(js|jsx)$/,        
                    // we do not want anything from node_modules to be compiled        
                    exclude: /node_modules/,        
                    use: ['babel-loader']      
                },      
                {       
                    test: /\.(css|scss)$/,        
                    use: [          
                        "style-loader", // creates style nodes from JS strings          
                        "css-loader", // translates CSS into CommonJS          
                        "sass-loader" // compiles Sass to CSS, using Node Sass by default        
                    ]      
                },      
                {        
                    test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,        
                    loaders: ['file-loader']      
                }    
            ]  
        },  
        plugins: [    
            new HtmlWebpackPlugin({      
                template: path.join(__dirname,'src','index.html')    
            })  
        ]
};
/*

Entry & output - 
    * These are used to tell our server what has to be compiled,
      from where (entry: path.join(__dirname,’src’,’index.js’),). 
    * It also tells where to put the outputted compiled version 
    (output — the folder and the filename)

mode -  This is the mode of our output. EX : "webpack": "NODE_ENV=production webpack",

resolve - This is used so that we can import anything from src folder in relative paths instead of absolute ones.

devServer - This tells the webpack-dev-server what files are needed to be served. 

plugins - here we set what plugins we need in our app.
*/