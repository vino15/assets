import { Transition } from 'react-transition-group';
import React, { useState, Fragment } from 'react';

const Fade = () => {

    const [inProp, setInProp] = useState(false);
    return (
        <Fragment>
            <Transition in={inProp} timeout={300}>
                {
                    state => (
                        <div className={`transition-${state}`}>
                            I'm a fade Transition!
                        </div>
                    )
                }
            </Transition>
            <button onClick={() => setInProp(!inProp)}>
                Click to Enter
            </button>

        </Fragment>
    );
}

export default Fade