import React, { useState } from 'react';
import { Container, Button, Alert } from 'reactstrap';
import { SwitchTransition, CSSTransition,Transition } from 'react-transition-group';
// import FadeTransition from './index'

export default function App() {
    const [state, setState] = useState(false);
    console.log(state, 'STAW')
    return (
        <>
            <SwitchTransition name="fade" mode="out-in">
                {/* <CSSTransition 
                    key={state ? "Goodbye, world!" : "Hello, world!"}
                    classNames='alert'
                    addEndListener={(node, done) => node.addEventListener("transitionend", done, false)}
                >
                    <button onClick={() => setState(state => !state)}>
                        {state ? "Goodbye, world!" : "Hello, world!"}
                    </button>
                </CSSTransition> */}

                <Transition 
                    key={state ? "Goodbye, world!" : "Hello, world!"}
                    // classNames='tra'
                    timeout={100}
                    addEndListener={(node, done) => node.addEventListener("transitionend", done, false)}
                >
                    {
                    className => 
                        <button  
                            className={`transition-${className}`}  
                            onClick={() => setState(state => !state)}
                        >
                            {state ? "Goodbye, world!" : "Hello, world!"}
                        </button>

                    }
                </Transition>
            </SwitchTransition>
        </>
    )
}