const withSass = require('@zeit/next-sass')
const withCSS = require("@zeit/next-css");
module.exports = withCSS(
    withSass({
        env: {
            BASE_URL: 'https://services.sair.in/'
        },
        webpack(config, options) {
            config.module.rules.push({
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000000,
                        fallback: 'responsive-loader',
                        quality: 85
                    }
                }
            })

            return config
        }
    })
);