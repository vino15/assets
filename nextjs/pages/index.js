import React from 'react'
import Head from 'next/head'
import Alphabets from '../components/Alphabets'
import Primarylayout from '../layout/primarylayout'
import '../assets/scss/main.scss'
import ReactTransition from '../components/ReactTransition'
import CSSTransition from '../components/ReactTransition/CSSTransition'
import SwitchTransition from '../components/ReactTransition/SwitchTransition'

const Home = () => (
  <div>
    
    <Head>
      <title>Home</title>
      <link rel="icon" href="/favicon.ico" />
      <i className="icon-custom-play"></i>
    </Head>

    <Primarylayout>
      {/* <Alphabets/> */}
      {/* <ReactTransition/> */}
      <SwitchTransition/>
      {/* <CSSTransition/> */}
    </Primarylayout>
 
  </div>
)

export default Home
