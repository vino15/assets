import AOS from 'aos';
import 'aos/dist/aos.css'; // You can also use <link> for styles
import React, { useEffect } from 'react';
const WithAos = () => {
    useEffect(() => {
        AOS.init()
    }, [])
    return (
        <div>
            {
                Array(10).fill().map(() => Math.round(Math.random() * 20)).map(barNumber => (
                    <div className="bar"data-aos="fade-down-right">
                        <div>
                            {
                                barNumber
                            }
                        </div>
                    </div>
                ))
            }
            

        </div>
    )
}

export default WithAos