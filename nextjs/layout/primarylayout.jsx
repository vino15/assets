import React from 'react'
import {Header , Footer} from '../components/common/'


const Primarylayout = ({children}) => (
  <>
     <Header />
      {children}
     <Footer />
  </>
)

export default Primarylayout
